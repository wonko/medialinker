#!/bin/bash

root="$1"
target="$2"

function sample(){
	file="$1"
	fname=$(basename "$1")
	dd if="$file" of="$target/$fname" bs=10M count=1 
	}
export -f sample
export  target
find $root -size +500M  -exec bash -c 'sample "$0"' {}  \;


