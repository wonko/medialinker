#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 2016 M01 6

@author: wonko
'''
import ConfigParser
import os, re, errno

from kaa import metadata
import tmdb3



class User_Interaction(object):
    ui = None
    CONSOLE_PLAIN = 1
    CONSOLE_CURSES = 2
    X_TK = 3
    
    @staticmethod
    def __not_implemented():
        print ('Method not implemented')
        
    TYPES = {CONSOLE_PLAIN: __not_implemented, CONSOLE_CURSES:__not_implemented, X_TK:__not_implemented}
    
    @staticmethod
    def get_ui():
        if User_Interaction.ui is None:
            User_Interaction.ui = User_Interaction()
        return User_Interaction.ui
    
    def __init__(self):
        if User_Interaction.ui:
            print('Error ui is already instatiated')
        self.display_type = User_Interaction.CONSOLE_PLAIN  # only implemented by now
        
    
class select_from_list(object):
        INITS = User_Interaction.TYPES.copy()
        SHOWS = User_Interaction.TYPES.copy()
        DEFAULT_CANCEL = 'defcnc'
        DEFAULT_FIRST = 'defrst'
        
        def init_console_plain(self, selection_list):
            self.out=unicode(errors='replace')
            self.out = unicode.join(u'',[u'----\n',self.text_above.decode('utf-8'),u'\n'])
            self.selection = {}
            self.selector = 1
            self.defaultkey = None
            if self.defaultitem:
                if self.defaultitem == self.DEFAULT_CANCEL:
                    self.defaultkey = '0'
                if self.defaultitem == self.DEFAULT_FIRST:
                    self.defaultkey = '1'
            while len(selection_list) > 0:
                text = selection_list.pop(0)
                self.selection[self.selector.__str__()] = text
                if text == self.defaultitem:
                    self.defaultkey = self.selector.__str__()
                self.selector += 1
            for s in sorted(self.selection.iterkeys()):
                self.out += '[' + s + '] ' + self.selection[s] + '\n'
            if self.buttons['cancel']:
                self.out += '\n[0] Cancel/Skip\n'
            self.cursor = self.text_below
            if self.defaultkey:
                self.cursor += ' [' + self.defaultkey + ']'
            self.cursor += ':'
        def show_console_plain(self):
            
            print (self.out)
            readin = raw_input(self.cursor)
            if self.defaultkey and readin == '':
                readin = self.defaultkey
            if readin in self.selection:
                return self.selection[readin]
            else:
                return None
             
        INITS[User_Interaction.CONSOLE_PLAIN] = init_console_plain
        SHOWS[User_Interaction.CONSOLE_PLAIN] = show_console_plain
    
        def __init__(self, listing, defaultitem=None, text_above='', text_below='', buttons={'cancel':True}):
            self.ui = User_Interaction.get_ui()
            self.listing = listing
            self.defaultitem = defaultitem
            self.text_above = text_above
            self.text_below = text_below
            self.buttons = buttons
            if isinstance(self.listing, dict):
                self.INITS[self.ui.display_type](self, self.listing.keys())
            else:
                self.INITS[self.ui.display_type](self, self.listing)
            
        
        def show(self):
            if isinstance(self.listing, dict):
                selected = self.SHOWS[self.ui.display_type](self)
                if selected:
                    return self.listing[selected]
            else:
                return self.SHOWS[self.ui.display_type](self)
            
   

class movie_file(object):
    
    REGX_YEAR = re.compile('(.*)(\()([0-9][0-9][0-9][0-9])(\).*)')
    REGX_TITLE = re.compile('.+?(?=[\[\(]|720|1080)')
    REGX_LANG = re.compile('\[([A-Z]{2,3})\]')
    REGX_IMDB = re.compile('tt\d{7}')
    REGX_COLLECTION = re.compile('collection',re.IGNORECASE)
    

    def __init__(self, file_path, file_name, usecache=True):
        self.quality = None
        self.audio_languages = set()
        self.imdb = None
        self.year = None
        self.title = None
        self.genres = set()
        self.sequel = None
        self.tmdb = None
        self.alt_titles = {}
        self.abs_filename = os.path.join(file_path, file_name)
        self._get_meta_from_filename(file_name)
        self._get_meta_from_file(self.abs_filename)
        self._identify_via_tmdb(True)
        self._get_meta_from_tmdb()
    
    def _get_meta_from_filename(self, file_name):
        self.title , self.ext = os.path.splitext(file_name)
        title = self._reg_match(self.REGX_TITLE, file_name, 0)
        if title:
            self.title = title
        for l in self._reg_match(self.REGX_LANG, file_name, 1, default=[]):
                self.audio_languages.add(l.lower())
        self.imdb = self._reg_match(self.REGX_IMDB, file_name, 0)
        self.year = self._reg_match(self.REGX_YEAR, file_name, 3)
        
                
    def _get_meta_from_file(self, mediafile):
        media_infos = {}
        media_infos['lang'] = set()
        infos = metadata.parse(mediafile)
        
        # code 'inspired' by https://github.com/NaPs/Kolekto/blob/master/kolekto/datasources/mediainfos.py
        # Set the quality of the video depending on its definition:
        if not infos or not hasattr(infos, 'video') or len(infos.video) <1 :
            return 
        if infos.video[0].width < 1280:
            self.quality = u'SD'
        elif 1280 <= infos.video[0].width < 1920:
            self.quality = u'720p'
        else:
            self.quality = u'1080p'
        
        for audio in infos.audio:
            if audio.langcode:
                self.audio_languages.add(audio.langcode.lower())
            elif audio.language:
                self.audio_languages.add(audio.language.lower())
    
    def _identify_via_tmdb(self, interactive=True):
        print (self.abs_filename)
        if self.imdb:
            self.tmdb = tmdb3.Movie.fromIMDB(self.imdb)
            return
        if interactive:
            result = tmdb3.searchMovie(self.title, year=self.year)
            select = {}
            for r in result:
                s_text = r._printable_name() + u' ---- '
                for alt in r.alternate_titles:
                    s_text += u'['+alt.country+u']:'+alt.title+u' '
                select[s_text] = r
            if len(select)>0:
                s = select_from_list(select, select_from_list.DEFAULT_FIRST, text_above=self.abs_filename, text_below='pls choose')
                self.tmdb = s.show()

    def _get_meta_from_tmdb(self):
        if not self.tmdb:
            self._identify_via_tmdb(False)
        if self.tmdb:
            self.title = self.tmdb.originaltitle
            for alt in self.tmdb.alternate_titles:
                self.alt_titles[alt.country] = alt.title
            for genre in self.tmdb.genres:
                self.genres.add(genre.name)

            if not self.tmdb.collection == u'':
                self.sequel = self.REGX_COLLECTION.sub(u'', self.tmdb.collection.name).strip()
            self.year = self.tmdb.releasedate.year.__str__()
            self.imdb = self.tmdb.imdb

    def _reg_match(self, pattern, in_string, group=None, default=''):
        m = pattern.search(in_string)
        if m is None:
            return default
        else:
            if group is not None:
                if isinstance(default, list):
                    return m.groups(group)
                return m.group(group)
        return None
    
    
    def normalized_filename(self, lang=None):
        title = self.title.encode('ascii',errors='ignore')
        n = title.replace('/','-')
        if self.year:
            n += '(' + self.year + ')'
        if self.imdb:
            n += '{' + self.imdb + '}'
        for l in self.audio_languages:
            n += u'[' + l + u']'
        if self.quality:
            n += self.quality
        n += self.ext
        return n
    
    def _sanity(self):
        pass
        # size, extension
    
    def __str__(self, *args, **kwargs):
        out = 'Title: ' + self.title
        out += '\nYear: ' + self.year
        out += '\nLanguages: ' + self.audio_languages.__str__()
        out += '\nQuality: ' + self.quality
        out += '\nIMDB: ' + self.imdb
        out += '\nAlternative Titles' + self.alt_titles.__str__()
        out += '\nGenres: ' + self.genres.__str__()
        return out

class MediaLinker(object):

    conf_defaults = {}
    conf_defaults['source_dir'] = '/storage/roadrunner/backup/import'
    conf_defaults['target_dir'] = '/storage/roadrunner/backup/movies'
    conf_defaults['files_dir'] = 'files'
    conf_defaults['unknown_dir'] = 'unknown'
    conf_defaults['doubles_dir'] = 'doubles'
    conf_defaults['lang_dir'] = 'by-language'
    conf_defaults['quality_dir'] = 'by-quality'
    conf_defaults['genre_dir'] = 'by-genre'
    conf_defaults['year_dir'] = 'by-year'
    conf_defaults['sequels_dir'] = 'sequels'
    conf_defaults['actors_dir'] = 'by-actor'
    conf_defaults['tmdb_api_key'] = ''
    conf_defaults['tmdb_cache_file'] = 'tmdb3.cache'  # relative paths are put in /tmp
    
    config = ConfigParser.SafeConfigParser(conf_defaults)
    
    def __init__(self):
        self.config.read('medialinker.conf')
        self.checkdirs()
        tmdb3.set_key(self.config.get('global', 'tmdb_api_key'))
        tmdb3.set_cache(filename=self.config.get('global', 'tmdb_cache_file'))

    def mkdir_p(self,path):
        try:
            os.makedirs(path)
            print('mkdir ' + path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                    pass
            else: raise

    def checkdirs(self):
        target_path = self.config.get('global', 'target_dir')
        if not target_path.endswith('/'):
                target_path += '/'
        for key in self.conf_defaults.keys(): 
            if key.endswith('_dir'):
                path = self.config.get('global', key)
                if not path.endswith('/'):
                    path += '/'
                if not path.startswith('/'):
                    path = target_path + path
                self.config.set('global', key, path)
                self.mkdir_p(path)

    def mk_link(self,dir_name, dir_value, new_filename, target_fname):
        path = self.config.get('global', dir_name) + dir_value + u'/'
        self.mkdir_p(path)
        link = path + target_fname
        if not os.path.islink(link):
            os.link(new_filename, link)
            #print ('linking ' + link)
            

    def move(self,movie,target_dir='files_dir'):
        success = True
        new_filename = os.path.join(self.config.get('global', target_dir), movie.normalized_filename())
        #new_filename = new_filename.decode('utf-8')
        if os.path.isfile(new_filename):
            success = False
            new_filename = os.path.join(self.config.get('global', 'doubles_dir'), movie.normalized_filename())
            new_filename = new_filename.decode('utf-8')
            num = 1
            nf = new_filename
            while os.path.isfile(new_filename):
                new_filename = nf + u'-'+num.__str__()
                num += 1
        print(u'moving ' + movie.abs_filename.decode('utf-8') + u' to ' + new_filename)
        os.rename(movie.abs_filename, new_filename)
        movie.abs_filename = new_filename
        return success
                        

    def link(self,movie):
        if movie.year:
            self.mk_link('year_dir', movie.year, movie.abs_filename, movie.normalized_filename())
        for lang in movie.audio_languages:
            langname = movie.normalized_filename(lang)
            self.mk_link('lang_dir', lang, movie.abs_filename, langname)
        for genre in movie.genres:
            self.mk_link('genre_dir', genre, movie.abs_filename, movie.normalized_filename())
        if movie.sequel:
            self.mk_link('sequels_dir', movie.sequel, movie.abs_filename, movie.normalized_filename())
        if movie.quality:
            self.mk_link('quality_dir', movie.quality, movie.abs_filename, movie.normalized_filename())

    def movies(self):
        for root, dirnames, file_names in os.walk(self.config.get('global', 'source_dir')):
            for file_name in file_names:
                movie = movie_file(root, file_name)
                if movie.imdb:
                    if self.move(movie):
                        self.link(movie)
                else:
                    self.move(movie, 'unknown_dir')
                    
                    
if __name__ == '__main__':
    m = MediaLinker()
    m.movies()

        
        
